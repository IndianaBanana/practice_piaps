package com.banana.practice.controller;

public interface IObserver {
    void update();
}
