package com.banana.practice.controller;

import com.banana.practice.model.IModel;
import com.banana.practice.model.IObservable;
import com.banana.practice.model.TspSolve;
import com.banana.practice.view.IView;
import com.banana.practice.view.action.ViewActions;

public class Controller implements IObserver {
    private IModel graph;
    private IView appView;
    private IObservable observable;

    public Controller(IModel graph, IView appView) {
        this.graph = graph;
        this.appView = appView;
        this.observable = (IObservable) graph;
        this.observable.addObserver(this);

        this.appView.registerActions(
                new ViewActions(this::addGraph)
        );
    }

    public void run() {
        appView.handle();
    }

    public void addGraph(int[][] graph) {
        this.graph.solveTSP(graph);
    }

    public TspSolve getSolveTSP() {
        return graph.getTspSolve();
    }

    @Override
    public void update() {
        appView.showTSPSolve(getSolveTSP());
    }
}
