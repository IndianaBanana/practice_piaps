package com.banana.practice.model;

import com.banana.practice.controller.IObserver;

import java.util.*;

public class GraphModel implements IModel, IObservable {
    private final List<IObserver> observers;
    private int[][] graph;
    private TspSolve tspSolve;

    public GraphModel() {
        observers = new ArrayList<>();
    }

    // Вспомогательный метод для нахождения всех перестановок пути
    private List<List<Integer>> permute(List<Integer> nums) {
        List<List<Integer>> results = new ArrayList<>();
        if (nums.isEmpty()) {
            results.add(new ArrayList<>());
            return results;
        }

        Integer firstElement = nums.remove(0);
        List<List<Integer>> remainingPermutations = permute(nums);
        for (List<Integer> permutation : remainingPermutations) {
            for (int i = 0; i <= permutation.size(); i++) {
                List<Integer> temp = new ArrayList<>(permutation);
                temp.add(i, firstElement);
                results.add(temp);
            }
        }
        nums.add(0, firstElement);
        return results;
    }

    // Метод для нахождения минимальной стоимости пути
    private int findMinCost(int[][] graph, List<Integer> path) {
        int cost = 0;
        for (int i = 0; i < path.size() - 1; i++) {
            cost += graph[path.get(i)][path.get(i + 1)];
        }
        // Возвращение к начальному городу
        cost += graph[path.get(path.size() - 1)][path.get(0)];
        return cost;
    }

    // Основной метод решения задачи коммивояжёра, возвращающий минимальную стоимость и путь
    @Override
    public void solveTSP(int[][] graph) {
        int n = graph.length;
        List<Integer> cities = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            cities.add(i);
        }

        List<List<Integer>> permutations = permute(cities);
        int minCost = Integer.MAX_VALUE;
        List<Integer> minPath = null;
        for (List<Integer> path : permutations) {
            int currentCost = findMinCost(graph, path);
            if (currentCost < minCost) {
                minCost = currentCost;
                minPath = new ArrayList<>(path);
            }
        }

        this.tspSolve = new TspSolve(minCost, minPath);
        notifyObservers();
    }

    @Override
    public TspSolve getTspSolve() {
        return tspSolve;
    }

    @Override
    public void addObserver(IObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        observers.remove(observer);
    }

    public void notifyObservers() {
        for (IObserver observer : observers) {
            observer.update();
        }
    }
}