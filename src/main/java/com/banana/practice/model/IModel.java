package com.banana.practice.model;

public interface IModel {
    void solveTSP(int[][] graph);
    TspSolve getTspSolve();
}