package com.banana.practice.model;

import com.banana.practice.controller.IObserver;

public interface IObservable {
    void notifyObservers();
    void addObserver(IObserver observer);
    void removeObserver(IObserver observer);
}
