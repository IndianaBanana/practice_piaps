package com.banana.practice.model;

import java.util.List;

public class TspSolve {
    private int cost;
    private List<Integer> path;

    TspSolve(int cost, List<Integer> path) {
        this.cost = cost;
        this.path = path;
    }

    public int getCost() {
        return cost;
    }

    public List<Integer> getPath() {
        return path;
    }
}
