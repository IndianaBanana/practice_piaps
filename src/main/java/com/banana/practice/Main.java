package com.banana.practice;

import com.banana.practice.controller.Controller;
import com.banana.practice.model.GraphModel;
import com.banana.practice.model.IModel;
import com.banana.practice.view.AppGUIView;
import com.banana.practice.view.IView;

public class Main {
    public static void main(String[] args) {
        IModel graph = new GraphModel();
        IView view = new AppGUIView();

        Controller controller = new Controller(graph, view);
        controller.run();
    }
}