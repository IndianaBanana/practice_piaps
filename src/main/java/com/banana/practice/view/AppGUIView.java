package com.banana.practice.view;

import com.banana.practice.model.TspSolve;
import com.banana.practice.view.action.IViewActions;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.StringJoiner;

public class AppGUIView implements IView {
    private JFrame frame;
    private JTextField numCitiesField;
    private JPanel matrixPanel;
    private JButton solveButton;
    private int numCities;
    private JTextField[][] distanceFields;
    private IViewActions viewActions;

    private void initWindow() {
        frame = new JFrame("Решение задачи коммивояжёра");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);
        frame.setLayout(new BorderLayout());
        frame.setLocationRelativeTo(null);

        JPanel topPanel = new JPanel();
        topPanel.add(new JLabel("Введите количество городов:"));
        numCitiesField = getJTextFieldWithValidation();
        topPanel.add(numCitiesField);
        JButton generateButton = new JButton("Далее");
        generateButton.addActionListener(new GenerateButtonListener());
        topPanel.add(generateButton);
        frame.add(topPanel, BorderLayout.NORTH);

        matrixPanel = new JPanel();
        frame.add(new JScrollPane(matrixPanel), BorderLayout.CENTER);

        solveButton = new JButton("Получить решение");
        solveButton.setEnabled(false);
        solveButton.addActionListener(new SolveButtonListener());
        frame.add(solveButton, BorderLayout.SOUTH);

        frame.setVisible(true);
    }

    public void handle() {
        initWindow();
    }

    public void registerActions(IViewActions viewActions) {
        this.viewActions = viewActions;
    }

    private JTextField getJTextFieldWithValidation() {
        JTextField jTextField = new JTextField(5);
        jTextField.setHorizontalAlignment(JTextField.CENTER);
        jTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                String value = jTextField.getText();
                if (e.getKeyChar() >= '0' && e.getKeyChar() <= '9' || e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                    jTextField.setEditable(true);
                } else {
                    jTextField.setEditable(false);
                }
            }
        });
        return jTextField;
    }

    private class GenerateButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            numCities = Integer.parseInt(numCitiesField.getText());
            if (numCities <= 2) {
                JOptionPane.showMessageDialog(frame, "Число городов должно быть больше 2.");
                return;
            } else if (numCities > 10) {
                JOptionPane.showMessageDialog(frame, "Число городов должно быть меньше или равно 10.");
                return;
            }

            matrixPanel.removeAll();
            matrixPanel.setLayout(new GridLayout(numCities + 1, numCities + 1));

            distanceFields = new JTextField[numCities][numCities];

            for (int i = 0; i <= numCities; i++) {
                for (int j = 0; j <= numCities; j++) {
                    if (i == 0 && j == 0) {
                        matrixPanel.add(new JLabel(""));
                    } else if (i == 0) {
                        matrixPanel.add(new JLabel("Город " + j, SwingConstants.CENTER));
                    } else if (j == 0) {
                        matrixPanel.add(new JLabel("Город " + i, SwingConstants.CENTER));
                    } else if (i == j) {
                        matrixPanel.add(new JLabel("0", SwingConstants.CENTER));
                    } else {
                        distanceFields[i - 1][j - 1] = getJTextFieldWithValidation();
                        matrixPanel.add(distanceFields[i - 1][j - 1]);
                    }
                }
            }

            solveButton.setEnabled(true);
            matrixPanel.revalidate();
            matrixPanel.repaint();
        }
    }

    private class SolveButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int[][] distances = new int[numCities][numCities];
            for (int i = 0; i < numCities; i++) {
                for (int j = 0; j < numCities; j++) {
                    if (i == j) {
                        distances[i][j] = 0;
                    } else {
                        distances[i][j] = Integer.parseInt(distanceFields[i][j].getText());
                    }
                }
            }
            viewActions.getAddGraphConsumer().accept(distances);
        }
    }

    public void showTSPSolve(TspSolve solve) {
        var cost = solve.getCost();
        var path = solve.getPath();

        StringJoiner sj = new StringJoiner(" → ");
        for (var it : path) {
            sj.add(String.format("Город %d", it + 1));
        }

        JOptionPane.showMessageDialog(frame,
                String.format("""
                        Решение задачи коммивояжёра:
                        Лучший путь: %s
                        Минимальная цена: %d""", sj, cost));
    }
}
