package com.banana.practice.view;

import com.banana.practice.model.TspSolve;
import com.banana.practice.view.action.IViewActions;

public interface IView {
    void showTSPSolve(TspSolve data);
    void registerActions(IViewActions viewActions);
    void handle();
}
