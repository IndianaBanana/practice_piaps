package com.banana.practice.view.action;

import java.util.List;
import java.util.function.Consumer;

public class ViewActions implements IViewActions {
    private Consumer<int[][]> addGraphConsumer;

    public ViewActions(Consumer<int[][]> addGraphConsumer) {
        addGraphRegisterAction(addGraphConsumer);
    }


    @Override
    public void addGraphRegisterAction(Consumer<int[][]> addGraphConsumer) {
        this.addGraphConsumer = addGraphConsumer;
    }

    @Override
    public Consumer<int[][]> getAddGraphConsumer() {
        return addGraphConsumer;
    }
}
