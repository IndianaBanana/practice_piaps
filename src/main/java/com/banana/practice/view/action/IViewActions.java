package com.banana.practice.view.action;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public interface IViewActions {
//    void addVertexRegisterAction(Consumer<String> addVertexConsumer);
//    void addEdgeRegisterAction(TriConsumer<String, String, Integer> addEdgeTriConsumer);
//    Consumer<String> getAddVertexConsumer();
//    TriConsumer<String, String, Integer> getAddEdgeTriConsumer();
    void addGraphRegisterAction(Consumer<int[][]> addVertexConsumer);
    Consumer<int[][]> getAddGraphConsumer();
}
