package com.banana.practice;

import com.banana.practice.model.GraphModel;
import org.jgrapht.traverse.DepthFirstIterator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class UnitTest {

//    @Test
//    public void checkTSPSolve() {
//        var graphModel = new GraphModel();
//
//        graphModel.addVertex("Orel");
//        graphModel.addVertex("Moscow");
//        graphModel.addVertex("Piter");
//        graphModel.addVertex("Tula");
//
//        graphModel.addEdge("Orel", "Moscow", 400);
//        graphModel.addEdge("Moscow", "Piter", 600);
//        graphModel.addEdge("Orel", "Piter", 1500);
//        graphModel.addEdge("Orel", "Tula", 180);
//        graphModel.addEdge("Tula", "Moscow", 190);
//        graphModel.addEdge("Piter", "Tula", 888);
//
//        System.out.println(graphModel.solveTSP());
//    }
//
//    @Test
//    public void checkTSPSolveThrowsRuntimeException() {
//        var graphModel = new GraphModel();
//
//        graphModel.addVertex("Orel");
//        graphModel.addVertex("Moscow");
//        graphModel.addVertex("Piter");
//        graphModel.addVertex("Tula");
//
//        graphModel.addEdge("Orel", "Moscow", 400);
//        graphModel.addEdge("Moscow", "Piter", 600);
//        graphModel.addEdge("Orel", "Piter", 1500);
//        graphModel.addEdge("Orel", "Tula", 180);
//        graphModel.addEdge("Tula", "Moscow", 190);
////        graphModel.addEdge("Piter", "Tula", 888);
//
//        // Граф должен быть полносвязным
//        Assertions.assertThrows(RuntimeException.class, () -> System.out.println(graphModel.solveTSP()));
//    }

    @Test
    public void checkControllerAndViewLink() {
//        var controller = new Controller();
//
//        controller.addVertex("Orel");
//        controller.addVertex("Moscow");
//        controller.addVertex("Piter");
//
//        controller.addEdge("Orel", "Moscow", 360);
//        controller.addEdge("Moscow", "Piter", 600);
//
//        controller.showGraph();
    }

    @Test
    public void checkGraphModel() {
        var graphModel = new GraphModel();
        System.out.println(graphModel.addVertex("1"));
        System.out.println(graphModel.addVertex("2"));
        graphModel.addEdge("1", "2", 10);

        var x = graphModel.getGraph().getEdge(new Vertex("1"), new Vertex("2"));
        System.out.printf("%s -> %s : %f\n",
                graphModel.getGraph().getEdgeSource(x).getName(),
                graphModel.getGraph().getEdgeTarget(x).getName(),
                graphModel.getGraph().getEdgeWeight(x));

        System.out.println(graphModel.removeVertex("1"));

        Iterator<Vertex> iterator = new DepthFirstIterator<>(graphModel.getGraph());
        while (iterator.hasNext()) {
            Vertex vertex = iterator.next();
            System.out.println(vertex);
        }
    }
}
